// Autofocus search input field on dropdown open
const searchNavItem = document.getElementById('search-nav-item');
if (searchNavItem) {
    searchNavItem.addEventListener('shown.bs.dropdown', function () {
        document.getElementById('minisearch-input').focus();
    });
}
