// Define elements
const optionSelectors = document.getElementsByClassName('product-container');

if (optionSelectors.length > 0) {
    // Listen for change on option select fields
    Array.from(optionSelectors).forEach((el) => {
        const productInfo = el.querySelector('.product-info');
        el.querySelector('.product-info-main').appendChild(productInfo);
        const productCTA = el.querySelector('.product-cta a');
        productCTA.className = 'btn btn-outline-primary';
    });
}
