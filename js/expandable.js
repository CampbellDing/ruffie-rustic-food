// Define elements
const optionSelectors = document.getElementsByClassName('expandable');

if (optionSelectors.length > 0) {
    const handleExpand = function (el, button, toggle) {
        const height = el.scrollHeight;
        const isCollapsed = toggle ? el.style.height === '150px' : false;
        el.style.height = isCollapsed ? height + 30 + 'px' : '150px';
        isCollapsed
            ? el.classList.add('expanded')
            : el.classList.remove('expanded');
        button.textContent = isCollapsed ? 'See less' : 'See more';
    };

    // Listen for change on option select fields
    Array.from(optionSelectors).forEach((el) => {
        el.style.height = '150px';
        let button = document.createElement('button');
        button.textContent = 'See more';
        button.className = 'expandable-btn';
        button.addEventListener('click', () => handleExpand(el, button, true));
        el.appendChild(button);
        window.addEventListener('resize', () => handleExpand(el, button));
    });
}
