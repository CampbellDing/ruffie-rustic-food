// Define elements
const optionSelectors = document.getElementsByClassName('storelocator');

if (optionSelectors.length > 0) {
    var locations = [];

    const labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    // const imagePath = document
    //     .querySelector('#clusterImage')
    //     .getAttribute('src');
    let map, autocomplete, infowindow;

    var getMarkers = (locations) =>
        locations.map((location, i) => {
            const marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(location.Latitude),
                    lng: parseFloat(location.Longitude),
                },
                title: location.name,
                label: { text: labels[i % labels.length], color: '#000' },
                icon: {
                    path: 'M10.27,30.75l8.87-15.37a10.25,10.25,0,1,0-17.75,0Z',
                    fillColor: '#000',
                    fillOpacity: 1,
                    strokeWeight: 0,
                    rotation: 0,
                    anchor: new google.maps.Point(15, 30),
                    labelOrigin: new google.maps.Point(10, 12),
                },
                map: map,
            });

            marker.addListener('click', () => {
                infowindow.setContent(`<h5>${location.Name}</h5>
        <p>${location.AddressLine1}, ${location.Suburb}, ${location.State}, ${location.Postcode}</p>
        <p>${location.Phone}</p>
        `);
                infowindow.open({
                    anchor: marker,
                    map,
                    shouldFocus: false,
                });
            });
            return marker;
        });

    window.getPostalLocation = () => {
        const place = autocomplete.getPlace();

        if (!place.geometry || !place.geometry.location) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert(`No details available for input: ${place.name}`);
            return;
        }

        console.log(place);

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        getWoolisStores({
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
        });
    };

    var getCurrentLocation = () => {
        navigator.geolocation.getCurrentPosition(
            (pos) => {
                var coords = {
                    lat: pos.coords.latitude,
                    lng: pos.coords.longitude,
                };
                map.panTo(coords);
                // The marker, positioned at current
                new google.maps.Marker({
                    position: coords,
                    map: map,
                });

                getWoolisStores(coords);
            },
            (err) => console.warn(`ERROR(${err.code}): ${err.message}`)
        );
    };

    var getWoolisStores = (coords) => {
        console.log(coords);
        fetch(
            'https://www.woolworths.com.au/apis/ui/StoreLocator/Stores?Max=10&Division=SUPERMARKETS,CALTEXWOW,METROCALTEX&Facility=&latitude=' +
                coords.lat +
                '&longitude=' +
                coords.lng
        )
            .then((response) => response.json())
            .then((data) => {
                // console.log(data, window.stores);
                const markers = getMarkers(
                    //filter the woolis api returned stores against the store list provided by ruffie food from window.stores
                    data.Stores.filter((store) =>
                        window.stores
                            ? window.stores.some(
                                  (st) => st.phone.trim() === store.Phone.trim()
                              )
                            : false
                    )
                );
                // // Add a marker clusterer to manage the markers.
                // new MarkerClusterer(map, markers, {
                //   imagePath
                // });
                zoomToFitMarkers(markers);
            });
    };

    var zoomToFitMarkers = (markers) => {
        var bounds = new google.maps.LatLngBounds();
        markers.map((marker) => bounds.extend(marker.getPosition()));
        map.fitBounds(bounds); //auto-zoom
    };

    window.initMap = () => {
        const center = { lat: -37.8076491, lng: 144.96052580000003 };
        map = new google.maps.Map(document.getElementById('map'), {
            center,
            zoom: 8,
            styles: [
                {
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#f5f5f5',
                        },
                    ],
                },
                {
                    elementType: 'labels.icon',
                    stylers: [
                        {
                            visibility: 'off',
                        },
                    ],
                },
                {
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#616161',
                        },
                    ],
                },
                {
                    elementType: 'labels.text.stroke',
                    stylers: [
                        {
                            color: '#f5f5f5',
                        },
                    ],
                },
                {
                    featureType: 'administrative.land_parcel',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#bdbdbd',
                        },
                    ],
                },
                {
                    featureType: 'poi',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#eeeeee',
                        },
                    ],
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#757575',
                        },
                    ],
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#e5e5e5',
                        },
                    ],
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#9e9e9e',
                        },
                    ],
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#ffffff',
                        },
                    ],
                },
                {
                    featureType: 'road.arterial',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#757575',
                        },
                    ],
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#dadada',
                        },
                    ],
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#616161',
                        },
                    ],
                },
                {
                    featureType: 'road.local',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#9e9e9e',
                        },
                    ],
                },
                {
                    featureType: 'transit.line',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#e5e5e5',
                        },
                    ],
                },
                {
                    featureType: 'transit.station',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#eeeeee',
                        },
                    ],
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [
                        {
                            color: '#c9c9c9',
                        },
                    ],
                },
                {
                    featureType: 'water',
                    elementType: 'geometry.fill',
                    stylers: [
                        {
                            color: '#bfdcea',
                        },
                    ],
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {
                            color: '#9e9e9e',
                        },
                    ],
                },
            ],
        });

        infowindow = new google.maps.InfoWindow();

        getMarkers(locations);

        const input = document.getElementById('postcode');
        const options = {
            componentRestrictions: { country: 'au' },
            fields: ['address_components', 'geometry'],
            origin: center,
            strictBounds: false,
            // types: ["(regions)"],
        };
        autocomplete = new google.maps.places.Autocomplete(input, options);
        getCurrentLocation();
    };

    // window.addEventListener('load', () => {
    // initMap();
    // });

    // Create the script tag, set the appropriate attributes
    var script = document.createElement('script');
    script.src =
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyC533g4ivMcuN_RTkzX7DMmfWuw91fBybk&libraries=places&v=weekly&callback=initMap';
    script.async = true;

    // Append the 'script' element to 'head'
    document.head.appendChild(script);
}
