// Handle sticky-top
var stickyNavbar = function () {
    if (document.getElementById('navbar').classList.contains('navbar-sticky')) {
        document
            .getElementById('shopify-section-navbar')
            .classList.add('sticky-top');
    }
};

var mobileMenuToggle = function () {
    if (document.getElementById('navbar').querySelector('.navbar-toggler')) {
        var navTogglerDOM = document.getElementById('navbar').querySelector('.navbar-toggler');

        navTogglerDOM.addEventListener('click', function () {
            document.querySelector('#navbar .container').classList.toggle('menu-open');
        });
    }
};

stickyNavbar();
mobileMenuToggle();

// Listen for changes in Shopify Theme Editor
document.addEventListener('shopify:section:load', function (event) {
    if (event.detail.sectionId === 'navbar') {
        stickyNavbar();
        mobileMenuToggle();
    }
});
