## Prerequisites

-   [Theme Kit](https://shopify.dev/tools/theme-kit/getting-started)
-   node.js
-   gulp

## Steps

1. `npm i`
2. `npm run dev`
3. Create `config.yml` (provided by appscore)
4. `theme open --env={ENV}` - to open the theme you want to edit
5. `theme watch --notify=/tmp/theme.update --allow-live --env={ENV}`
    - _Make sure to specify the `--env={ENV}`_

## Highlighted features:

-   Powered by [Bootstrap framework](https://getbootstrap.com/) (v5)
-   Developed respecting [Shopify themes requirements](https://shopify.dev/tutorials/review-theme-store-requirements)
-   All elements are fully accessible with [aria attributes](https://www.w3.org/WAI/standards-guidelines/aria/)
-   No Javascript framework dependencies (e.g jQuery)
-   Support for [native image lazy-loading](https://web.dev/native-lazy-loading/)
-   PageSeed score 96/100 [check results](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fks-bootshop.myshopify.com%2F&tab=desktop)
-   All Shopify required homepage sections (~20)
-   All Shopify templates (cart, product, etc) have their corresponding settings
-   Product layout option grid or list
-   Ajax add to cart
-   Reccomended products section [Learn more](https://shopify.dev/tutorials/develop-theme-recommended-products)

## Syncing the theme you're working on with staging/prod

1. Merge staging/prod to the branch youre working on
2. `theme deploy --env={ENV}`
